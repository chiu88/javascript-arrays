/*
Nicely done here, Shawn. Your application functions as required.
You've got the names I needed to see as well as the comments I needed
to see.  Nothing wrong here
20/20
*/
// function to populate the provinces into the select box with id cboProv
function loadProvinces() {

    // creating an array and populating it
    var provArray = new Array("Alberta", "British Columbia", "Manitoba", "New Brunswick", "Newfoundland and Labrador", "Northwest Territories", "Nova Scotia", "Nunavut", "Ontario", "Prince Edward Island", "Quebec", "Saskatchewan", "Yukon");

    // creating a variable to hold the element with id 'cboProv'
    var selectBox = document.getElementById('cboProv');
    var defaultOption = document.createElement('option');
    defaultOption.value = "";
    defaultOption.innerHTML = "-Select-";
    selectBox.appendChild(defaultOption);

    // populates the select box with id cboProv with the values of the array
    for (i=0; i<provArray.length; i++) {
        var option = document.createElement('option');
        option.value = provArray[i];
        option.innerHTML = provArray[i];
        selectBox.appendChild(option);
    }
}

// function to validate the user input
function validateForm() {
    var selectBox = document.getElementById('cboProv');
    var txtNameBox = document.getElementById('txtName');
    var txtEmailBox = document.getElementById('txtEmail');

    if(selectBox.selectedIndex == "0") {
        alert("Please select a province!");
        selectBox.focus();
        return

    } else if(txtNameBox.value == "") {
        alert("Please enter your name!!");
        txtNameBox.focus();
        return
    } else if(txtEmailBox.value == "") {
        alert("Please enter your email!!!");
        txtEmailBox.focus();
        return
    } else {
        alert("congratulations!!! you have entered all the information successfully");
    }
}
